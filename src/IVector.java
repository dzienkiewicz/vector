public interface IVector {

    double getX();

    double getY();

    void setX(double x);

    void setY(double y);

    void setType(VectorType type);

    VectorType getType();

    double scalar(Vector v);
    IVector addVectors(Vector v);
    IVector subtractVectors(Vector v);
    double scalar(Vector3D v);
    IVector addVectors(Vector3D v);
    IVector subtractVectors(Vector3D v);
}
