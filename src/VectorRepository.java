import java.util.ArrayList;
import java.util.List;

public class VectorRepository {
    private List<IVector> vectors;

    public VectorRepository(){
        this.vectors=new ArrayList<>();
    }

    public void addVector(IVector vector){
        vectors.add(vector);
    }

    public List<IVector> getVectors() {
        return vectors;
    }

    public void setVectors(List<IVector> vectors) {
        this.vectors = vectors;
    }
}
