public abstract class CalculateVector
{
    static IVector addVectors(IVector v1, IVector v2){
        if(v2.getType()==VectorType.Vector2D)
            return v1.addVectors((Vector) v2);
        else if (v2.getType()==VectorType.Vector3D){
            System.out.println(v1.getClass());
            return v1.addVectors((Vector3D) v2);}
    return null;
    }
    static IVector subtractVectors(IVector v1, IVector v2) {
        if(v2.getType()==VectorType.Vector2D)
            return v1.subtractVectors((Vector) v2);
        else if (v2.getType()==VectorType.Vector3D)
            return v1.subtractVectors((Vector3D) v2);
        return null;
    }
    static double scalarVectors(IVector v1, IVector v2) {
        if(v2.getType()==VectorType.Vector2D)
            return v1.scalar((Vector) v2);
        else if (v2.getType()==VectorType.Vector3D)
            return v1.scalar((Vector3D) v2);
        return 0;
    }
}
