import java.util.Optional;
import java.util.Scanner;

public abstract class VectorCreator {
public static Optional<IVector> create(){
    System.out.println("1. 2D");
    System.out.println("2. 3D");
    Scanner sc=new Scanner(System.in);
    int menu= sc.nextInt();
    switch (menu){
        case 1:{
            System.out.println("X: ");
            double x = sc.nextDouble();
            System.out.println("Y: ");
            double y = sc.nextDouble();

            return Optional.of(Vector.create(x,y));}
        case 2: {
            System.out.println("X: ");
            double x = sc.nextDouble();
            System.out.println("Y: ");
            double y = sc.nextDouble();
            System.out.println("Z: ");
            double z = sc.nextDouble();

            return Optional.of(Vector3D.create(x, y, z));}
        default:
            return Optional.empty();
        }
    }
}
