import java.util.Optional;
import java.util.Scanner;

public abstract class Menu {
    public static void print(VectorRepository repository){
        Scanner scanner=new Scanner(System.in);
        while (true) {
            System.out.println("----MENU----");
            System.out.println("1. Dodaj wektor");
            System.out.println("2. Wyświetl wektor");
            System.out.println("3. Wyświetl wszystkie wektory");
            System.out.println("4. Dodawanie");
            System.out.println("5. Odejmowanie");
            System.out.println("6. Iloczyn skalarny");

            int option=scanner.nextInt();
            switch (option){
                case 1:
                    Optional<IVector> opt=VectorCreator.create();
                    opt.ifPresent(repository::addVector);
                    VectorPrinter.printVectors(repository.getVectors());
                case 2:
            }
        }
    }
}
