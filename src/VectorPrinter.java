import java.util.List;

public abstract class VectorPrinter {
    static void printVector(IVector vector){
        if(vector.getType()==VectorType.Vector2D)
            System.out.println("Vector 2D x="+vector.getX()+" y="+vector.getY());
        else if(vector.getType()==VectorType.Vector3D) {
            Vector3D vector3D = (Vector3D) vector;
            System.out.println("Vector 3D x=" + vector3D.getX() + " y=" + vector3D.getY() + " z=" + vector3D.getZ());
        }
        }
        static void printVectors(List<IVector> repo){
        for (IVector vector:repo){
            printVector(vector);
        }
        }
}
