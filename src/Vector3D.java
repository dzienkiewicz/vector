import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Vector3D extends Vector {
    private double z;

    Vector3D(double x, double y, double z) {
        super(x,y);
        setType(VectorType.Vector3D);
        this.z=z;
    }
    Vector3D(){
        this.type=VectorType.Vector3D;
    }
    public static Vector3D create(double x, double y,double z){
        Vector3D v=new Vector3D();
        v.x=x;
        v.y=y;
        v.z=z;
        return v;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }


    @Override
    public IVector addVectors(Vector v) {
        Vector3D vector3D = new Vector3D(v.x, v.y, 0);
        return vector3D.addVectors(new Vector3D(this.x,this.y,this.z));
    }
    @Override
    public IVector addVectors(Vector3D v){
        return new Vector3D(this.x+v.getX(),this.y+v.getY(),this.z+v.getZ());
    }
    @Override
    public IVector subtractVectors(Vector v) {
        Vector3D vector3D = Vector3D.create(v.x, v.y, 0);
        return vector3D.subtractVectors(create(this.x,this.y,this.z));
    }

    @Override
    public IVector subtractVectors(Vector3D v) {
        return Vector3D.create(this.x - v.getX(),this.y- v.getY(),this.z - v.getZ());
    }

    @Override
    public double scalar(Vector3D v) {
        return this.x * v.getX() + this.y * v.getY() + this.z * v.getZ();
    }

    @Override
    public double scalar(Vector v) {
        Vector3D vector3D = Vector3D.create(v.x, v.y, 0);
        return vector3D.scalar(create(this.x,this.y,this.z));
    }
}
