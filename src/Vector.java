import static java.lang.Math.*;

public class Vector implements IVector {
    protected double x;
    protected double y;
    protected VectorType type;

    public Vector(){
        this.type=VectorType.Vector2D;
    }
    public Vector(double x, double y){
        this.x=x;
        this.y=y;
        this.type=VectorType.Vector2D;
    }
    public static Vector create(double x, double y){
        Vector v=new Vector();
        v.x=x;
        v.y=y;
        return v;
    }
    @Override
    public double scalar(Vector v) {
        return this.x*v.getX()+this.y*v.getY();
    }

    @Override
    public IVector addVectors(Vector v) {

        return new Vector(this.x+v.getX(),this.y+v.getY());
    }

    @Override
    public IVector subtractVectors(Vector v) {

        return new Vector(this.x-v.getX(),this.y-v.getY());
    }
    @Override
    public double scalar(Vector3D v){
        Vector3D vector3D = new Vector3D(this.x,this.y,0);
        return 0;
    }
    @Override
    public IVector addVectors(Vector3D v) {
        Vector3D vector3D = new Vector3D(this.x,this.y,0);
        return new Vector3D(vector3D.getX()+v.getX(),vector3D.getY()+v.getY(),vector3D.getZ()+v.getZ());
    }
    @Override
    public IVector subtractVectors(Vector3D v) {
        Vector3D vector3D = new Vector3D(this.x,this.y,0);
        return new Vector3D(vector3D.getX()-v.getX(),vector3D.getY()-v.getY(),vector3D.getZ()-v.getZ());
    }
    @Override
    public double getX() {

        return x;
    }
    @Override
    public void setX(double x) {

        this.x = x;
    }
    @Override
    public double getY() {

        return y;
    }

    @Override
    public void setY(double y) {

        this.y = y;
    }
    @Override
    public VectorType getType() {

        return type;
    }
    @Override
    public void setType(VectorType type) {

        this.type = type;
    }

}
